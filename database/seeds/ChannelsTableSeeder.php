<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = [
            [
                'name' => 'php',
                'slug' => 'php'
            ],
            [
                'name' => 'laravel',
                'slug' => 'laravel'
            ],
        ];
 
        foreach ($channels as $channel)
            DB::table('channels')->insert($channel);
    }
}