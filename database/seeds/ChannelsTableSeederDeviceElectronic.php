<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ChannelsTableSeederDeviceElectronic extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = [
            [
                'type' => 'xperia',
                'brand' => 'sony',
                'series' => 'XR',
                'year' => '2015'
                               
            ],
            [
                'type' => 'MI',
                'brand' => 'XIAOMI',
                'series' => 'MI A1',
                'year' => '2018'
            ],
        ];
 
        foreach ($channels as $channel)
            DB::table('channels')->insert($channel);
    }
}