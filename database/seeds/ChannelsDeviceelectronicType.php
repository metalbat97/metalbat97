<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ChannelsDeviceelectronicType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = [
            [
                'type' => 'Notebook'
                               
            ],
            [
                'type' => 'Tablet',
            ],
            [
                'type' => 'Router',
            ],

            [
                'type' => 'Switch',
            ],

        ];
 
        foreach ($channels as $channel)
            DB::table('deviceelectronictype')->insert($channel);
    }
}