<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $threads = [
            [
                'user_id' => 1,
                'channel_id' => 1,
                'title' => 'A random sentence',
                'body' => 'Some lorem ipsum'
            ],
            [
                'user_id' => 1,
                'channel_id' => 2,
                'title' => 'A random sentence',
                'body' => 'Some lorem ipsum'
            ],
        ];
 
        foreach ($threads as $thread)
            DB::table('threads')->insert($thread);
    }
}